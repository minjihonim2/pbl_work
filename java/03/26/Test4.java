package day03;

public class Test4 {
	// 메소드 정의 방법
	// 접근제한자 리턴타입 메소드명 (매개변수들..){}
	public void printSum(int number) {
		int oddSum =0;
		int evenSum = 0;
		for(int i =1; i <= number; i++) {
			if(i % 2 == 0)
				evenSum += i;
			else
				oddSum += i;
		}
		System.out.println("짝수의 합 : "+evenSum);
		System.out.println("홀수의 합 : "+oddSum);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum = 0;
		for(int i =1; i<11; i++) {
			if (i % 2 ==0)
				System.out.println(i);
		}
		Test4 test = new Test4();
		test.printSum(100);
	}
	
}