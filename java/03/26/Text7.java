package day03;

public class Text7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//배열. []
		int ii;
		ii = 10;
		
		int iarray[]; //선언
		iarray = new int[3]; //생성
		
		int iarr[] = new int[5]; //선언과 동시에 생성!!
		
	//	int iarr2[]= {1,2,3,4}; //선언,생성, 초기화까지~
		int iarr2[] = new int[4];
		iarr2[0] = 1;
		iarr2[1] = 2;
		iarr2[2] = 3;
		iarr2[3] = 4;
	
		for(int i = 0; i < iarr2.length; i++) {
			System.out.println(iarr2[i]);
		}
		
		int iarr3[] = new int[10];
		for(int i = 0; i<iarr3.length; i++) {
			iarr3[i] = i+1;
			System.out.println(iarr3[i]);
		}
		
		int iarr4[] = new int[] {1,2,3,4};
		int iarr5[] = {1,2,3,4};
		
	
		//for( 타입 변수명 | : 반복되는 자료구조(ex 배열) )
		for(int value:iarr5) {
			System.out.println(value);
		}
	}
}