package day03;

import java.util.*;

public class PrimeDetect {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Scanner stdin = new Scanner(System.in);
			System.out.println("소수인지 판단할 숫자:");
			
			int divisor = 2;
			int num = stdin.nextInt();
			boolean isPrime = true;
			
			while(divisor < num) {
				if(num % divisor++ == 0)
					isPrime = false;
				break;
			}
	

	if(isPrime)

	{
		System.out.println(num + "은 소수입니다.");
	} else
	{
		System.out.println(num + "은 소수가아닙니다.");
	}
}
}