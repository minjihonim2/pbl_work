package day04;

public class ArrayUtils {
	
	static double [] intToDouble( int[] source ) {
		double[] resultArr = new double[source.length];
		for(int index = 0; index < source.length; index++ ) {
			resultArr[index] = source[index];
		}
		return resultArr;
	}
	
	static int [] doubleToint( double[] source ) {
		int[] resultArr = new int[source.length];
		for(int index = 0; index < source.length; index++) {
			resultArr[index] = (int)source[index];
		}
		return resultArr;
	}
	
	static int [] concat( int[] s1, int[] s2) {
		int[] resultArr = new int[s1.length+s2.length];
		int resultIndex = 0;
		for(int i = 0; i < s1.length; i++) {
			resultArr[resultIndex++] = s1[i];
		}
		for(int i = 0; i < s2.length; i++) {
			resultArr[resultIndex++] = s2[i];
		}
		return resultArr;
	}
	
	public static void main(String[] args) {
		int[] s1 = {1, 2, 3};
		int[] s2 = {4, 5, 6};
		int[] result = ArrayUtils.concat(s1, s2);
		for (int i : result) {
			System.out.println(i);
		}
	
	
	double[] rArr = ArrayUtils.intToDouble(s1);
	for (double d : rArr) {
		System.out.println(d);
	}
	}
}