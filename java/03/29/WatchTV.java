package day04;

public class WatchTV {

	   public static void main(String[] args) {
	      TV tv = new TV();
	      //TV는 현재 꺼진상태, 7번채널, 볼륨20임.
	      
	      tv.powerOn();
	      
	      tv.channelUp();
	      
	      tv.getChannel();
	      
	      tv.setChannel(5);
	      
	      tv.channelUp();
	      
	      tv.getChannel();
	      
	      tv.getVolume();
	      
	      tv.isPower();
	      
	      tv.powerOff();
	      
	      tv.channelUp();
	   }
}