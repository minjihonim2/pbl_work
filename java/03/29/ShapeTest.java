package day04.shape;

public class ShapeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Point p = new Point();
		p.setX(2);
		p.setY(5);
		
		Point p2 = new Point();
		p2.setX(10);
		p2.setY(23);
		
		p.show();
		p2.show();
		
		Point p3 = new Point(20,40);
		p3.show();
		
		Point p4 = new Point();
		p4.show();
		
		p4.show(true);
		p4.show(false);
		
		Point b = new ColorPoint(3, 4, "red");
		b.show();
		
	}

}
