package day04;

public class Goods {
	private String name;
	private int price;
	private int countStock;
	private int countSold;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getcountStock() {
		return countStock;
	}
	public void setcountStock(int countStock) {
		this.countStock = countStock;
	}
	public int getcountSold() {
		return countSold;
	}
	public void setcountSold(int countSold) {
		this.countSold = countSold;
	}
	public int getCountStock() {
		return countStock;
	}
	public void setCountStock(int countStock) {
		this.countStock = countStock;
	}
	public int getCountSold() {
		return countSold;
	}
	public void setCountSold(int countSold) {
		this.countSold = countSold;
	}
	@Override
	public String toString() {
		return "Goods \n상품이름:" + name + "\n상품가격:" + price + "\n재고수량:" + countStock + "\n팔린수량:" + countSold;
	}
	
}

