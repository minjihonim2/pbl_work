package day04.shape;

public class ColorPoint extends Point {
	String color;
	int x;
	int y;
public ColorPoint(int x, int y, String color) {
		this.x = x;
		this.y = y;
		this.color = color;
}

public void show() {
	// this 불가능 this.getX() 는 가능
	System.out.printf("좌표[x=%d,y=%d,color=%s]에 점을 그렸습니다. \n",x,y,color);
	}

}