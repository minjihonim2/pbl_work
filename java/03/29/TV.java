package day04;

public class TV {

	   int channel = 7;
	   int volume = 20;
	   boolean power = false;

	   public int channelUp() {

	      if (power) {
	         this.channel = this.channel + 1;
	      } else {
	         System.out.println("티비가 꺼진 상태입니다");
	      }

	      return channel;
	   }

	   public int channelDown() {
	      if (power) {
	         this.channel = channel - 1;
	      } else {
	         System.out.println("티비가 꺼진 상태입니다");
	      }
	      return channel;
	   }

	   public void getChannel() {
	      System.out.printf("채널은 %d번 입니다\n", channel);

	   }

	   public int setChannel(int channel) {
	      this.channel = channel;
	      System.out.printf("채널은 %d번 입니다\n", channel);
	      return channel;
	   }

	   public int volumeUp(int volume) {
	      if (power) {
	         this.volume = volume + 1;
	      }

	      else {
	         System.out.println("티비가 꺼진 상태입니다");
	      }
	      return volume;
	   }

	   public int volumeDown(int volume) {
	      if (power) {
	         this.volume = volume - 1;
	      } else {
	         System.out.println("티비가 꺼진 상태입니다");
	      }
	      return volume;
	   }

	   public int getVolume() {

	      System.out.println("현재 볼륨값은 : " + volume);
	      return volume;
	   }

	   public boolean powerOn() {
	      this.power = true;
	      System.out.println("티비가 켜졌습니다");
	      return power;
	   }

	   public boolean powerOff() {
	      this.power = false;
	      System.out.println("티비가 꺼졌습니다");
	      return power;
	   }

	   public void isPower() {
	      if (power) {
	         System.out.println("현재 티비는 켜져있습니다");
	      } else {
	         System.out.println("현재 티비는 꺼져있습니다");
	      }

	   }

	}
