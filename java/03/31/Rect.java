package day06;

public class Rect {
	private int width;
	private int height;
	public Rect(int width, int height) {
		this.width = width;
		this.height = height;
	}
	@Override
	public boolean equals(Object obj) { //매개변수 안에서 불러오는게 로딩, 매개변수 밖에서 불러오는것 라이딩
		// TODO Auto-generated method stub
		if(obj instanceof Rect) {
			Rect r = (Rect)obj;
			if( width*height == r.width*r.height) {
				return true;
			}else {
				return false;
			}
		}else
			return false;
	}
}
//	public boolean equals( Rect p ) {
//		if( width*height == p.width *p.height) {
//			return true;
//		}else {
//			return false;
//		}
//	}
//}
