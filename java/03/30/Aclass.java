package day05;

abstract class Aclass {
	public abstract void Amethod(); // 추상 메소드 
}
abstract class Bclass extends Aclass { 
	public abstract void Bmethod(); // 추상 클래스를 상속받은 추상클래스
}

public class Cclass extends Bclass {
	public void Amethod () { ... } // 반드시 구현해야함
	public void Bmethod () { ... } 
}
