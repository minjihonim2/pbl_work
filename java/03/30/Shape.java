package day05;

abstract public class Shape {
	private double w;
	private double h;

	public abstract void draw();

	public abstract double calculateArea();

	public Shape() {

	}

	public Shape(double w, double h) {
		this.w = w;
		this.h = h;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}
}
