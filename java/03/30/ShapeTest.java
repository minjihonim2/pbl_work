package day05;

public class ShapeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape s = new Rect(4,5);
		s.draw();
		System.out.println(s.calculateArea());
		
		Shape s2 = new Circle(5);
		s2.draw();
		System.out.println(s2.calculateArea());
	}

}
