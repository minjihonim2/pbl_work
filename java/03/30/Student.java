package day05;

public class Student extends Person{
	private String grade;
	private String major;
	public Student( String name ) {
		super(name);
	}
	public String getGrade() {
			return grade;
		}
	public void setGrade(String grade) {
			this.grade = grade;
	}
}
