package day05;
class Parent2{
	public void m1(){
		System.out.println("m1");
	}
	
}
class Child2 extends Parent2{
	public void m2() {
		System.out.println("m2");
	}
	
}
public class Test3 {
	public static void testMethod(Object obj) {
		System.out.println("객체는 모두 들어와!!! ");
	}
	public static void testMethod2(Parent2 p) {
		System.out.println("Parent 들만 들어와");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Parent2 p = new Parent2();
		//자식 타입으로 부모를 가리키는 것은 안된다!!!
		Child2 c = new Child2();
		p = c; // 묵시적으로 형변환이 일어난 상태!!
		
		p = new Child2();
		c = (Child2)p; //묵시적인 형변환이다!!
		
		p = new Child2();
		p.m1();
//		p.m2();  p가 가리키는 인스턴스는 child2이므로 m2를 가지고 있음에도 사용할 수 없다!!!
		//그래서 필요한게 형변환!!!
		c = (Child2)p; // 명시적 형변환!
		c.m2();
		
		Parent2 p3 = new Parent2();
		
		if(p3 instanceof Child2) {
			Child2 c3 = (Child2)p3; //형변환이 가능한 경우는 타입과 상관없이 인스턴스가 child1일 경우 가능!!
		}
		
		testMethod("abc");
		testMethod(p3);
		testMethod(c);
//		testMethod2("abc");
		testMethod2(p3);
		testMethod2(c);
	}

}
