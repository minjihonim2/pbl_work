package day05;

public class MyClass implements BInter, CInter {

	@Override
	public void m1() {
		// TODO Auto-generated method stub

	}

	@Override
	public int m3() {
		// TODO Auto-generated method stub
		return 0;
	}
	public static void main(String[] args) {
		AInter a = new MyClass();
		a.m1();
		
		CInter c = new MyClass();
		
		BInter b = new MyClass();
		
		System.out.println(a instanceof MyClass);
		System.out.println(a instanceof AInter);
		System.out.println(a instanceof BInter);
		System.out.println(a instanceof CInter);
	}
}
