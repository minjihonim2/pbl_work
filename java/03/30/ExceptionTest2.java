package day05;

public class ExceptionTest2 {
	public static void main(String[] args) {
		int a = 3;
		double b;
		try {
			b = 100 / a; // java.lang.ArithmeticException 발생
			int[] iarr = { 1, 2, 3 };
			System.out.println(iarr[3]);
		} catch (ArithmeticException e) {
			System.out.println(e);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		System.out.println("Some more codes"); // 예외 발생으로 수행되지 않음

	}
}
