package day05;

import java.util.Calendar;

public class test4 {
	public static void main(String[] args) {
		AInter.sm();
		
		System.out.println(Math.PI); // 매스클래스에 스태틱하게 변수선언해놓음 , 
		//상수를 사용할 목적으로, 클래스의 속성과는 다르다
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.SIZE);
	}
}	// 인터페이스에서도 쓸 수 있다.
