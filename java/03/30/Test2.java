package day05;

class Parent {
		int i;
		public Parent() {
			System.out.println("parent 디폴트 생성자 실행 !! ");
		}
}
class Child extends Test2{
//	super(); 상속에서 생략되어져 있을거다.  / 부모의 생성자
	int j;
	public Child() {
		System.out.println("child의 디폴트, 생서자 실행!!");
	}
	
	public Child(int i, int j) {
		super(i);
		
	}
}

public class Test2{
	public static void main(string[] args) {
		Parent p = new Parent();
		Child c = new Child();
		
	}
}