package day05;

public interface AInter {
	int value = 10;
	
	public void m1() ; // 인터페이스는 기본은 선언만 해야한다.
	default public void mm3() {
		System.out.println("Ainter에서 구현!!");
	};
	
	static public void sm() {
		System.out.println("하하하");
	}
}
