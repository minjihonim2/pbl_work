package day02;

import java.util.Scanner;

public class ifExam07 {
	static void numbering(int a, int b, int c) {
		if (a<b && a<c) {
			System.out.println(a);
		}else if (b<c && b<a) {
			System.out.println(b);
		}else {
			System.out.println(c);
		}
	}
/*
 * 3개의 정수를 입력받아 조건연산자를 이용하여 입력받은 수들 중 최소값을 
 * 출력하는 프로그램을 작성하시오.
입력예:18 -5 10
출력예:-5
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("입력: ");
		int a = keyboard.nextInt();
		int b = keyboard.nextInt();
		int c = keyboard.nextInt();
		numbering(a, b, c);
	}
}