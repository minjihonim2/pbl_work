package day02;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int intValue=0;	// 변수 선언!!
		intValue = 10;	 //초기화, (지역변수는 반드시 초기화 해야함!!)
		System.out.println(intValue);
		intValue = 20;
		System.out.println(intValue);
//		intValue = "hello";
		String str ="hello";
		System.out.println(str);
		
		byte b = 100;
		intValue = b;
		System.out.println(intValue);
		b = (byte)intValue;
		char c = 'a';
		int i = c;
		System.out.println(i);
		float f = 10.0f;
		
	}

}
